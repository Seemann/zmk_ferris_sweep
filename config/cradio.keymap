// Copyright (c) 2022 The ZMK Contributors
// SPDX-License-Identifier: MIT

/*
 *  ____
 * / ___|  ___  ___ _ __ ___   __ _ _ __  _ __
 * \___ \ / _ \/ _ \ '_ ` _ \ / _` | '_ \| '_ \
 *  ___) |  __/  __/ | | | | | (_| | | | | | | |
 * |____/ \___|\___|_| |_| |_|\__,_|_| |_|_| |_|
 *
 * Seemann's keymap for the Ferris Sweep keyboard.
 *
 * OS must be set to German keyboard layout (because of the umlauts).
 *
 */

#include <behaviors.dtsi>
#include <dt-bindings/zmk/keys.h>
#include <dt-bindings/zmk/bt.h>

#include "keymap_german.h"

#define COMBO_TIMEOUT_MS     35
#define HRM_QUICK_TAP_MS     0
#define HRM_TAPPING_TERM_MS  250

/* Convert NAME parameters into a ZMK label string */
#define STRINGIFY(x) #x

/* simplify hold_tap on numpad for DWM tag switch */
#define numki(keycode) LG(LA(keycode)) keycode

/* Create a ZMK behavior without having to specify the name three times */
#define ZMK_BEHAVIOR(NAME, ...) \
        NAME: NAME { \
            __VA_ARGS__ \
        };

/* Mod-Morph helper */
#define MOD_MORPH(NAME, STANDARD, MORPHED, MODS) \
    ZMK_BEHAVIOR(NAME, \
        compatible = "zmk,behavior-mod-morph"; \
        #binding-cells = <0>; \
        bindings = <STANDARD>, <MORPHED>; \
        mods = <(MODS)>; \
    )

/* Mod-Morph that sends another key when Shift is held */
#define SHIFT_MORPH(NAME, LOWER, UPPER) \
    MOD_MORPH(NAME, LOWER, UPPER, MOD_LSFT|MOD_RSFT)

#define HRML(k1,k2,k3,k4) &hrm_l LCTRL k1 &hrm_l LGUI k2 &hrm_l LALT k3 &hrm_l LSHFT k4
#define HRMR(k1,k2,k3,k4) &hrm_r RSHFT k1 &hrm_r LALT k2 &hrm_r RGUI k3 &hrm_r RCTRL k4

// Layer names
#define BASE 0
#define ALTR 1
#define SYM  2
#define NUM  3
#define NAV  4
#define KEY  5

&caps_word {
    continue-list = <UNDERSCORE MINUS BACKSPACE DELETE DE_UNDS N1 N2 N3 N4 N5 N6 N7 N8 N9 N0>;
};

&lt {
    tapping-term-ms = <150>;
};


/ {
    behaviors {
        SHIFT_MORPH(mor_del, &kp BACKSPACE, &kp DELETE)
        SHIFT_MORPH(mor_dquot, &kp DE_SQT, &kp DE_DQT)
        SHIFT_MORPH(mor_coln, &kp LS(DE_SEMI), &end_semicln)

        mo_del: behavior_mo_del {
            compatible = "zmk,behavior-hold-tap";
            #binding-cells = <2>;
            flavor = "tap-preferred";
            tapping-term-ms = <200>;
            bindings = <&kp>, <&mor_del>;
        };

        mo_tap: hold_tap {
            compatible = "zmk,behavior-hold-tap";
            #binding-cells = <2>;
            flavor = "tap-preferred";
            tapping-term-ms = <200>;
            quick-tap-ms = <150>;
            global-quick-tap;
            bindings = <&mo>, <&mor_del>;
        };

        hrm_l: homerow_mods_left {
            compatible = "zmk,behavior-hold-tap";
            #binding-cells = <2>;
            tapping-term-ms = <HRM_TAPPING_TERM_MS>;
            quick-tap-ms = <HRM_QUICK_TAP_MS>;
            flavor = "balanced";
            bindings = <&kp>, <&kp>;
        };

        hrm_r: homerow_mods_right {
            compatible = "zmk,behavior-hold-tap";
            #binding-cells = <2>;
            tapping-term-ms = <HRM_TAPPING_TERM_MS>;
            quick-tap-ms = <HRM_QUICK_TAP_MS>;
            flavor = "balanced";
            bindings = <&kp>, <&kp>;
        };

    nk: number_dwm {
            compatible = "zmk,behavior-hold-tap";
            #binding-cells = <2>;
            tapping_term_ms = <250>;
            quick_tap_ms = <0>;
            flavor = "tap-preferred";
            bindings = <&kp>, <&kp>;
        };
    };

        //╭──────────┬──────────┬──────────┬──────────┬──────────╮   ╭──────────┬──────────┬──────────┬──────────┬──────────╮
        //│  0       │  1       │  2       │  3       │  4       │   │  5       │  6       │  7       │  8       │  9       │
        //├──────────┼──────────┼──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┼──────────┼──────────┤
        //│  10      │  11      │  12      │  13      │  14      │   │  15      │  16      │  17      │  18      │  19      │
        //├──────────┼──────────┼──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┼──────────┼──────────┤
        //│  20      │  21      │  22      │  23      │  24      │   │  25      │  26      │  27      │  28      │  29      │
        //╰──────────┴──────────┴──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┴──────────┴──────────╯
        //                                 |  30      |  31      |   |  32      |  33      |
        //                                 ╰──────────┴──────────╯   ╰──────────┴──────────╯
    combos {
        compatible = "zmk,combos";
        combo_alt {
            timeout-ms = <COMBO_TIMEOUT_MS>;
            key-positions = <0 1>;
            bindings = <&to ALTR>;
            layers = <BASE>;
        };
        combo_base {
            timeout-ms = <COMBO_TIMEOUT_MS>;
            key-positions = <0 1>;
            bindings = <&to BASE>;
            layers = <ALTR>;
        };
        combo_caps {
            timeout-ms = <COMBO_TIMEOUT_MS>;
            key-positions = <30 31>;
            bindings = <&caps_word>;
        };
        combo_lbkt {
            timeout-ms = <COMBO_TIMEOUT_MS>;
            key-positions = <26 27>;
            bindings = <&kp DE_LBKT>;
        };
        combo_rbkt {
            timeout-ms = <COMBO_TIMEOUT_MS>;
            key-positions = <27 28>;
            bindings = <&kp DE_RBKT>;
        };
        combo_lbrc {
            timeout-ms = <COMBO_TIMEOUT_MS>;
            key-positions = <6 7>;
            bindings = <&kp DE_LBRC>;
        };
        combo_rbrc {
            timeout-ms = <COMBO_TIMEOUT_MS>;
            key-positions = <7 8>;
            bindings = <&kp DE_RBRC>;
        };
        combo_lpar {
            timeout-ms = <COMBO_TIMEOUT_MS>;
            key-positions = <16 17>;
            bindings = <&kp DE_LPAR>;
        };
        combo_rpar {
            timeout-ms = <COMBO_TIMEOUT_MS>;
            key-positions = <17 18>;
            bindings = <&kp DE_RPAR>;
        };
        combo_labk {
            timeout-ms = <COMBO_TIMEOUT_MS>;
            key-positions = <11 12>;
            bindings = <&kp DE_LABK>;
        };
        combo_rabk {
            timeout-ms = <COMBO_TIMEOUT_MS>;
            key-positions = <12 13>;
            bindings = <&kp DE_RABK>;
        };
        combo_last_win {
            timeout-ms = <COMBO_TIMEOUT_MS>;
            key-positions = <13 14>;
            bindings = <&tmux_last_win>;
        };
        combo_prev_win {
            timeout-ms = <COMBO_TIMEOUT_MS>;
            key-positions = <1 2>;
            bindings = <&tmux_prev_win>;
        };
        combo_next_win {
            timeout-ms = <COMBO_TIMEOUT_MS>;
            key-positions = <2 3>;
            bindings = <&tmux_next_win>;
        };
    };

    macros {
        end_semicln: end_semicln {
            compatible = "zmk,behavior-macro";
            #binding-cells = <0>;
            tap-ms = <0>;
            wait-ms = <0>;
            bindings
            = <&macro_tap &kp END &kp LS(COMMA)>
            ;
        };
        tmux_last_win: tmux_last_win {
            compatible = "zmk,behavior-macro";
            #binding-cells = <0>;
            tap-ms = <0>;
            wait-ms = <0>;
            bindings
            = <&macro_tap &kp LA(A) &kp L>
            ;
        };
        tmux_prev_win: tmux_prev_win {
            compatible = "zmk,behavior-macro";
            #binding-cells = <0>;
            tap-ms = <0>;
            wait-ms = <0>;
            bindings
            = <&macro_tap &kp LA(A) &kp P>
            ;
        };
        tmux_next_win: tmux_next_win {
            compatible = "zmk,behavior-macro";
            #binding-cells = <0>;
            tap-ms = <0>;
            wait-ms = <0>;
            bindings
            = <&macro_tap &kp LA(A) &kp N>
            ;
        };
    };

    keymap {
        compatible = "zmk,keymap";
        base_layer {
            bindings = <
        //╭──────────┬──────────┬──────────┬──────────┬──────────╮   ╭──────────┬──────────┬──────────┬──────────┬──────────╮
        //│  Q       │  W       │  F       │  P       │  B       │   │  J       │  L       │  U       │  Y       │  ;       │
        //├──────────┼──────────┼──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┼──────────┼──────────┤
        //│  A       │  R       │  S       │  T       │  G       │   │  M       │  N       │  E       │  I       │ O        │
        //├──────────┼──────────┼──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┼──────────┼──────────┤
        //│  Z       │  X       │  C       │  D       │  V       │   │  K       │  H       │ ,        │ .        │ /        │
        //╰──────────┴──────────┴──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┴──────────┴──────────╯
        //                                 │  SPACE   │  TAB     │   │ BSPC/DEL │ ESC      │
        //                                 │  _NUM_   │  _NAV_   │   │ _NAV_    │ _SYM_    │
        //                                 ╰──────────┴──────────╯   ╰──────────┴──────────╯
            &kp Q      &kp W      &kp F      &kp P      &kp B          &kp J      &kp L      &kp U      &kp Z      &mor_coln
            HRML(A,        R,         S,         T)     &kp G          &kp M  HRMR(N,        E,         I,        O)
            &kp Y      &kp X      &kp C      &kp D      &kp V          &kp K      &kp H      &kp COMMA  &kp DOT    &kp DE_FSLH
                                        &lt NUM SPACE &lt NAV TAB      &mo_tap NAV 0 &lt SYM ESC
            >;
        };

        alternate_layer {
            bindings = <
        //                                                    Engrammer Layout
        //╭──────────┬──────────┬──────────┬──────────┬──────────╮   ╭──────────┬──────────┬──────────┬──────────┬──────────╮
        //│  B       │  Z       │  O       │  U       │  -       │   │  Y       │  L       │  D       │  W       │  V       │
        //├──────────┼──────────┼──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┼──────────┼──────────┤
        //│  C       │  I       │  E       │  A       │  .       │   │  Q       │  H       │  T       │  S       │  N       │ 
        //├──────────┼──────────┼──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┼──────────┼──────────┤
        //│  G       │  X       │  J       │  K       │  ,       │   │  /       │  R       │  M       │  F       │  P       │ 
        //╰──────────┴──────────┴──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┴──────────┴──────────╯
        //                                 │  SPACE   │  TAB     │   │ BSPC/DEL │ ESC      │
        //                                 │  _NUM_   │  _NAV_   │   │ _NAV_    │ _SYM_    │
        //                                 ╰──────────┴──────────╯   ╰──────────┴──────────╯
            &kp B      &kp Y      &kp O      &kp U      &kp DE_MINS    &kp Z         &kp L     &kp D      &kp W     &kp V
            HRML(C,        I,         E,         A)     &kp DOT        &kp Q    HRMR(H,        T,         S,        N)
            &kp G      &kp X      &kp J      &kp K      &kp COMMA      &kp DE_FSLH   &kp R     &kp M      &kp F     &kp P
                                        &lt NUM SPACE &lt NAV TAB      &mo_tap NAV 0 &lt SYM ESC
            >;
        };

        sym_layer {
            bindings = <
        //╭──────────┬──────────┬──────────┬──────────┬──────────╮   ╭──────────┬──────────┬──────────┬──────────┬──────────╮
        //│ !        │  '       │  "       │  ´       │ ?        │   │          │          │          │          │ ;        │
        //├──────────┼──────────┼──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┼──────────┼──────────┤
        //│ #        │  =       │  _       │  $       │ *        │   │ ^        │          │          │          │ Enter    │
        //├──────────┼──────────┼──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┼──────────┼──────────┤
        //│ &        │  |       │  -       │  +       │ ~        │   │ @        │          │ ,        │ .        │ \        │
        //╰──────────┴──────────┴──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┴──────────┴──────────╯
        //                                 │  :       │ %        │   │          │          │
        //                                 │  KEY     │          │   │          │          │
        //                                 ╰──────────┴──────────╯   ╰──────────┴──────────╯
       &kp DE_EXCL  &kp DE_SQT  &kp DE_DQT  &kp DE_ACUT &kp DE_QUES   &none        &none &none  &none  &trans
       &kp DE_HASH  &kp DE_EQL  &kp DE_UNDS &kp DE_DLR  &kp DE_STAR   &kp DE_CIRC  &none &none  &none  &kp ENTER
       &kp DE_AMPS  &kp DE_PIPE &kp DE_MINS &kp DE_PLUS &kp DE_TILD   &kp DE_AT    &none &trans &trans &kp DE_BSLH
                                        &lt KEY DE_COLN &kp DE_PERC   &none        &none
            >;
        };

        num_layer {
            bindings = <
        //╭──────────┬──────────┬──────────┬──────────┬──────────╮   ╭──────────┬──────────┬──────────┬──────────┬──────────╮
        //│          | Ö        |  Ä       |  Ü       |          │   │  -       │  7       │  8       │  9       │ :        │
        //├──────────┼──────────┼──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┼──────────┼──────────┤
        //│ ß        │ A        │  B       │  C       │ €        │   │  +       │  4       │  5       │  6       │ Enter    │
        //├──────────┼──────────┼──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┼──────────┼──────────┤
        //│          │ D        │  E       │  F       │          │   │  *       │  1       │  2       │  3       │ /        │
        //╰──────────┴──────────┴──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┴──────────┴──────────╯
        //                                 │          │          │   │  .       │  0       │
        //                                 │          │          │   │          │          │
        //                                 ╰──────────┴──────────╯   ╰──────────┴──────────╯
        &none      &kp DE_ODIA  &kp DE_ADIA &kp DE_UDIA  &none        &kp DE_MINS &nk numki(DE_7)   &nk numki(DE_8)   &nk numki(DE_9)   &kp DE_COLN
        HRML(DE_SS, A,          B,          C)           &kp DE_EURO  &kp DE_PLUS &nk numki(DE_4)   &nk numki(DE_5)   &nk numki(DE_6)   &kp ENTER
        &none      &kp D        &kp E       &kp F        &none        &kp DE_STAR &nk numki(DE_1)   &nk numki(DE_2)   &nk numki(DE_3)   &kp DE_FSLH
                                            &none        &none        &kp DOT     &lt KEY DE_0
            >;
        };

        nav_layer {
            bindings = <
        //╭──────────┬──────────┬──────────┬──────────┬──────────╮   ╭──────────┬──────────┬──────────┬──────────┬──────────╮
        //│ F1       │ F2       │ F3       │ F4       │ Vol +    │   │ Home     │          │          │ Ins      │ Del      │
        //├──────────┼──────────┼──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┼──────────┼──────────┤
        //│ F5       │ F6       │ F7       │ F8       │ Mute     │   │ Left     │ Down     │ Up       │ Right    │ End      │
        //├──────────┼──────────┼──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┼──────────┼──────────┤
        //│ F9       │ F10      │ F11      │ F12      │ Vol -    │   │ End      │ PgDn     │ PgUp     │          │          │
        //╰──────────┴──────────┴──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┴──────────┴──────────╯
        //                                 │          │          │   │          │          │
        //                                 ╰──────────┴──────────╯   ╰──────────┴──────────╯
            &kp F2     &kp F2     &kp F3     &kp F4     &kp K_VOL_UP   &kp HOME   &none      &none      &kp INS    &kp DEL
            HRML(F5,   F6,        F7,        F8)        &kp K_MUTE     &kp LEFT   HRMR(DOWN, UP,        RIGHT,     END)
            &kp F9     &kp F10    &kp F11    &kp F12    &kp K_VOL_DN   &kp END    &kp PG_DN  &kp PG_UP  &none      &none
                                             &none      &none          &none      &none
            >;
        };

        key_layer {
            bindings = <
        //╭──────────┬──────────┬──────────┬──────────┬──────────╮   ╭──────────┬──────────┬──────────┬──────────┬──────────╮
        //│ RESET    │          │          │          │PROFILE 0 │   │          │          │          │          │ RESET    │
        //├──────────┼──────────┼──────────┼──────────┼──────────┤   ├──────────┤ ─────────┼──────────┼──────────┼──────────┤
        //│BOOTLOADER│          │          │          │PROFILE 1 │   │          │          │          │          │BOOTLOADER│
        //├──────────┼──────────┼──────────┼──────────┼──────────┤   ├──────────┤──────────┼──────────┼──────────┼──────────┤
        //│          │          │ CLEAR BT │          │PROFILE 2 │   │          │          │          │          │          │
        //╰──────────┴──────────┴──────────┼──────────┼──────────┤   ├──────────┼──────────┼──────────┴──────────┴──────────╯
        //                                 ╰──────────┴──────────╯   ╰──────────┴──────────╯
            &sys_reset  &none     &none      &none     &bt BT_SEL 0    &none      &none      &none      &none      &sys_reset
            &bootloader &none     &none      &none     &bt BT_SEL 1    &none      &none      &none      &none      &bootloader
            &none       &none     &bt BT_CLR &none     &bt BT_SEL 2    &none      &none      &none      &none      &none
                                             &none     &none           &none      &none
            >;
        };
    };
};
