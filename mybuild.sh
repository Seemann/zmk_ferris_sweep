#!/bin/bash

WORKDIR=../zmk/app
CURRDIR=$(pwd)

BUILD_RIGHT="west build -d build/right -b nice_nano_v2 -- -DSHIELD=cradio_right -DZMK_CONFIG=/home/chris/Projekte/seemann-zmk/config"
BUILD_LEFT="west build -d build/left -b nice_nano_v2 -- -DSHIELD=cradio_left -DZMK_CONFIG=/home/chris/Projekte/seemann-zmk/config"

cd $WORKDIR

$BUILD_LEFT && $BUILD_RIGHT
if [ $? -eq 0 ]; then
    cp build/right/zephyr/zmk.uf2 $CURRDIR/sweep_right.uf2
    cp build/left/zephyr/zmk.uf2 $CURRDIR/sweep_left.uf2
fi

cd -

